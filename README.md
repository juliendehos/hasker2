# hasker2

*This project is still experimental.*

hasker2 (HASKell project managER) is a small command-line tool for creating a
Haskell project. It implements 3 commands: `init` (initialize a project), `app`
(add an executable in the project) and `lib` (add a module in the library of
the project and add a test file for this module).  The generated project can be
built using Nix+Cabal or using Stack. 


## Installation

- using Nix + Home-manager :

    - in the config file, add the package:

    ```
    (import (fetchTarball "https://gitlab.com/juliendehos/hasker2/-/archive/master/hasker2-master.tar.gz") {})
    ```

    - update config:

    ```
    home-manager switch
    ```


- using Nix:

```
nix-env -i -f https://gitlab.com/juliendehos/hasker2/-/archive/master/hasker2-master.tar.gz
```

- using Stack:

```
stack install

# then add $HOME/.local/bin to your PATH:
echo "export PATH=$PATH:$HOME/.local/bin" >> ~/.bashrc
source ~/.bashrc
```


## Usage

- get help:

```
hasker2 --help
```

- create a project:

```
hasker2 init myproj 
cd myproj
```

- build the project using Nix+Cabal:

```
nix-shell
cabal build
...
```

- build the project using Stack:

```
stack build
...
```

- add a lib/app:

```
hasker2 lib MyModule1
hasker2 lib Mod2.Sub1
hasker2 app app2
```

