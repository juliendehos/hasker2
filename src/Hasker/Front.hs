
{-# LANGUAGE OverloadedStrings #-}

module Hasker.Front where

import Hasker.Args
import Options.Applicative

argsParser :: Parser Args
argsParser = Args <$> cmdParser

cmdParser :: Parser Cmd
cmdParser = subparser
    (  command "init" (info cmdInit (progDesc "Init a project"))
    <> command "lib" (info cmdLib (progDesc "Add a module in the library"))
    <> command "app" (info cmdApp (progDesc "Add an application"))
    )

cmdInit :: Parser Cmd
cmdInit = CmdInit <$> argument str (metavar "ProjectName")

cmdLib :: Parser Cmd
cmdLib = CmdLib <$> argument str (metavar "ModuleName")

cmdApp :: Parser Cmd
cmdApp = CmdApp <$> argument str (metavar "ApplicationName")

get :: IO (Args)
get = execParser
    $ info (argsParser <**> helper)
           (fullDesc <> progDesc "HASKell project managER")

