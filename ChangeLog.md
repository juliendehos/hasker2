# Revision history for Hasker

## 0.2 -- 2019-10-02

* Prevent using invalid project/app/lib names.

## 0.1 -- 2019-09-23

* First version. Can init a Haskell project, add executables and add modules.

