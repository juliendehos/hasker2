
{-# LANGUAGE OverloadedStrings #-}

module Hasker.BackSpec (main, spec) where

import Test.Hspec

import Hasker.Back

main :: IO ()
main = hspec spec

spec :: Spec
spec = do

  describe "toUpperFirst" $ do
    it "toto" $ toUpperFirst "toto" `shouldBe` "Toto"
    it "Toto" $ toUpperFirst "Toto" `shouldBe` "Toto"
    it "totO" $ toUpperFirst "totO" `shouldBe` "TotO"
    it "TotO" $ toUpperFirst "TotO" `shouldBe` "TotO"
    it "t" $ toUpperFirst "t" `shouldBe` "T"
    it "T" $ toUpperFirst "T" `shouldBe` "T"
    it "[]" $ toUpperFirst "" `shouldBe` ""

  describe "checkName" $ do
    it "toto" $ checkName "toto" `shouldBe` Right "toto"
    it "Toto" $ checkName "Toto" `shouldBe` Right "Toto"
    it "toto1" $ checkName "toto1" `shouldBe` Right "toto1"
    it "to-to" $ checkName "to-to" `shouldBe` Left "it should have alphanum characters only"
    it "to_to" $ checkName "to_to" `shouldBe` Left "it should have alphanum characters only"
    it "to.to" $ checkName "to.to" `shouldBe` Left "it should have alphanum characters only"
    it "1toto" $ checkName "1toto" `shouldBe` Left "it should begin with an alphabetic character"

  describe "checkNameLib" $ do
    it "Toto" $ checkNameLib "Toto" `shouldBe` Right "Toto"
    it "Toto1" $ checkNameLib "Toto1" `shouldBe` Right "Toto1"
    it "To.To" $ checkNameLib "To.To" `shouldBe` Right "To.To"
    it "toto" $ checkNameLib "toto" `shouldBe` Left "all names should begin with an uppercase alphabetic character"
    it "To-To" $ checkNameLib "To-To" `shouldBe` Left "it should have alphanum characters or '.' only"
    it "to-to" $ checkNameLib "to-to" `shouldBe` Left "it should have alphanum characters or '.' only"
    it "toto1" $ checkNameLib "toto1" `shouldBe` Left "all names should begin with an uppercase alphabetic character"
    it "1toto" $ checkNameLib "1toto" `shouldBe` Left "all names should begin with an uppercase alphabetic character"

