
import Hasker.Front (get)
import Hasker.Back (run)

main :: IO ()
main = get >>= run

