{ pkgs ? import <nixpkgs> {} }:
let 
  drv = pkgs.haskellPackages.callCabal2nix "hasker2" ./. {};
in
  if pkgs.lib.inNixShell then drv.env else drv


